window.onload = init;

var windowWidth;

var headerTrigger, histoireTrigger, contactTrigger, prestaTrigger;

function init() {
    
    $("#linkToHistoire").click(function() {
        $('html,body').animate({
            scrollTop: $("#histoire").offset().top
        }, 800);
    });
    $("#linkToPresta").click(function() {
        $('html,body').animate({
            scrollTop: $("#prestations").offset().top
        }, 800);
    });
    $("#linkToContact").click(function() {
        $('html,body').animate({
            scrollTop: $("#contact").offset().top
        }, 800);
    });

    windowWidth = $(window).width();
    console.log(windowWidth);
    if(windowWidth >= 1280){
        headerTrigger = 100;
        histoireTrigger = 650;
        contactTrigger = 1500;
        prestaTrigger = 2550;
    } else {
        headerTrigger = 50;
        histoireTrigger = 350;
        contactTrigger = 1000;
        prestaTrigger = 1750;
    }

    changeCss();
}


window.addEventListener("scroll", changeCss , false);

function changeCss() {
    var headerEl = document.getElementById("header");
    if (this.scrollY > headerTrigger) {
        if (!headerEl.classList.contains('scrolled')) {
            headerEl.classList.add('scrolled');
        }
    } else {
        if (headerEl.classList.contains('scrolled')) {
            headerEl.classList.remove('scrolled');
        }
    }

    // HISTOIRE
    var histoireTxt = document.getElementById('histoireBlocTxt');
    if (this.scrollY > histoireTrigger) {
        histoireTxt.classList.add('visible');
    }

    // PRESTATION
    var prestationH1 = document.getElementById('prestations').getElementsByTagName('h1')[0];
    var choice1 = document.getElementById('prestations').getElementsByClassName('choice')[0];
    var choice2 = document.getElementById('prestations').getElementsByClassName('choice')[1];
    var choice3 = document.getElementById('prestations').getElementsByClassName('choice')[2];
    if (this.scrollY > contactTrigger) {
        prestationH1.classList.add('visible');
        setTimeout(function () {
            choice1.classList.add('visible');
        }, 500);
        setTimeout(function () {
            choice2.classList.add('visible');
        }, 1000);
        setTimeout(function () {
            choice3.classList.add('visible');
        }, 1500);
    }

    // CONTACT
    var contact = document.getElementById('contact')
    if (this.scrollY > prestaTrigger) {
        contact.classList.add('visible');
    }
}


function changePrestaChoice(e) {
    console.log(e);
    let newChoiceEl = e.target;
    if(!newChoiceEl.classList.contains('choiceName')) {
        newChoiceEl = newChoiceEl.parentNode;
    }
    document.querySelector('.choiceName.selected').parentNode.querySelector('.choiceDescr').classList.add('hidden');
    document.querySelector('.choiceName.selected').classList.remove('selected');
    
    newChoiceEl.classList.add('selected');
    newChoiceEl.parentNode.querySelector('.choiceDescr').classList.remove('hidden');
}


function selectFormule(e) {
    let selectedFormuleEl = e.target;
    if(!selectedFormuleEl.classList.contains('formule')) {
        selectedFormuleEl = selectedFormuleEl.parentNode;
    }
    selectedFormuleEl.parentNode.querySelectorAll('.formule').forEach(formuleEl => {
        if(formuleEl !== selectedFormuleEl){
            formuleEl.classList.remove('selectedFormule');
            formuleEl.classList.add('hiddenFormule');
        } else {
            formuleEl.classList.remove('hiddenFormule');
            formuleEl.classList.add('selectedFormule');
        }
    });
    console.log(selectedFormuleEl);
}

function closeFormuleExample() {
    document.querySelectorAll('.formule').forEach(formuleEl => {
        formuleEl.classList.remove('selectedFormule');
        formuleEl.classList.remove('hiddenFormule');
    });
}


function openBuffetOnglet(e) {
    console.log(e.target);
    if(!e.target.classList.contains('closeBuffetIcon')) {
        let selectedBuffetEl = e.target;
        if(!selectedBuffetEl.classList.contains('buffet')) {
            selectedBuffetEl = selectedBuffetEl.parentNode;
            if(!selectedBuffetEl.classList.contains('buffet')) {
                selectedBuffetEl = selectedBuffetEl.parentNode;
            }
        }
        selectedBuffetEl.parentNode.querySelectorAll('.buffet').forEach(buffetEl => {
            if(buffetEl !== selectedBuffetEl){
                buffetEl.classList.remove('selectedBuffet');
                buffetEl.classList.add('hiddenBuffet');
            } else {
                buffetEl.classList.remove('hiddenBuffet');
                buffetEl.classList.add('selectedBuffet');
            }
        });
    }
}
function closeBuffetOnglet(){
    document.querySelectorAll('.buffet').forEach(buffetEl => {
        buffetEl.classList.remove('hiddenBuffet');
        buffetEl.classList.remove('selectedBuffet');
    });
}

function goToContact() {
    $('html,body').animate({
        scrollTop: $("#contact").offset().top
    }, 800);
}